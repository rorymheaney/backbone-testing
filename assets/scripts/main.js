/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
	var Sage = {
		// All pages
		'common': {
	  		init: function() {
				// JavaScript to be fired on all pages

				$(document).foundation(); // Foundation JavaScript

// var SinglePostView = Backbone.View.extend({
//   template: _.template('single-post'),
//   render: function() {
//     this.$el.html(this.template(this.model.attributes));
//     return this;
//   }
// });

				var trythis = new wp.api.collections.Rack;
				//console.log(trythis)
				
				var postsCollection = new wp.api.collections.Posts();

				console.log(postsCollection.models);
				postsCollection.fetch();
				console.log(postsCollection.fetch( { data: { per_page: 25 } } ));
				var posts = new wp.api.collections.Posts();
				console.log(posts.hasMore);
				var promise = posts.fetch( {
					data: {
						include: posts.ids, // Include the passed ids
						_embed: true // Embed all the post details including media.
					}
				} );

				// Continue when the fetch completes.
				promise
					.done(function (argument) {
						console.log('done');
						_.each( posts.models, function( post ) {

							// Create a new view from the post.
							// var singlePost = new SinglePostView( { model: post.attributes } );
							//console.log(post.attributes)
							//console.log(singlePost);
							// Add the view to our container view.
							//collectionView.views.add( singlePost )
						} );
						if (posts.hasMore === null){
							console.log('keep going');
						} else {
							console.log('no more');
						}
					})
					.always(function(){
						console.log('always');
					})
					.fail(function() {
					    alert( "error" );
				  });

	  		},
	  		finalize: function() {
				// JavaScript to be fired on all pages, after page specific JS is fired
	  		}
		},
		// Home page
		'home': {
	  		init: function() {
				// JavaScript to be fired on the home page
		},
	  		finalize: function() {
				// JavaScript to be fired on the home page, after the init JS
	  		}
		},
		// About us page, note the change from about-us to about_us.
		'about_us': {
	  		init: function() {
				// JavaScript to be fired on the about us page
	  		}
		}
	};

	// The routing fires all common scripts, followed by the page specific scripts.
	// Add additional events for more control over timing e.g. a finalize event
	var UTIL = {
		fire: function(func, funcname, args) {
			var fire;
			var namespace = Sage;
			funcname = (funcname === undefined) ? 'init' : funcname;
			fire = func !== '';
			fire = fire && namespace[func];
			fire = fire && typeof namespace[func][funcname] === 'function';

			if (fire) {
			  namespace[func][funcname](args);
			}
		},
		loadEvents: function() {
			// Fire common init JS
			UTIL.fire('common');

			// Fire page-specific init JS, and then finalize JS
			$.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
				UTIL.fire(classnm);
				UTIL.fire(classnm, 'finalize');
			});

			// Fire common finalize JS
			UTIL.fire('common', 'finalize');
		}
	};

	// Load Events
  	$(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
